import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TakasComponent } from './takas.component';

describe('TakasComponent', () => {
  let component: TakasComponent;
  let fixture: ComponentFixture<TakasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TakasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TakasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
