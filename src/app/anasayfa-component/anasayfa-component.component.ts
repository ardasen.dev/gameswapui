import { Component, OnInit } from '@angular/core';
import {kullaniciDB} from "../models/kullanici";
import {kullaniciService} from "../Service/kullaniciService";
@Component({
  selector: 'app-anasayfa-component',
  templateUrl: './anasayfa-component.component.html',
  styleUrls: ['./anasayfa-component.component.css'],
  providers: [kullaniciService]
})
export class AnasayfaComponentComponent implements OnInit {
  kullanici : kullaniciDB[] = [];
  constructor(private kullaniciService:kullaniciService) { }

  ngOnInit(): void {
    this.kullaniciService.getKullanici().subscribe(data=>{
      this.kullanici=data;

    } );

  }

}
