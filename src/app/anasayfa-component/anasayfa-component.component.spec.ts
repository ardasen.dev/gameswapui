import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnasayfaComponentComponent } from './anasayfa-component.component';

describe('AnasayfaComponentComponent', () => {
  let component: AnasayfaComponentComponent;
  let fixture: ComponentFixture<AnasayfaComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnasayfaComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnasayfaComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
