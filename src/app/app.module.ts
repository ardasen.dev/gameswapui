import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { AnasayfaComponentComponent } from './anasayfa-component/anasayfa-component.component';
import { KutuphaneComponentComponent } from './kutuphane-component/kutuphane-component.component';
import { MarketComponent } from './market/market.component';
import { ProfilComponent } from './profil/profil.component';
import { TakasComponent } from './takas/takas.component';
import { GirisComponent } from './giris/giris.component';
import { AppRoutingModule } from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    AnasayfaComponentComponent,
    KutuphaneComponentComponent,
    MarketComponent,
    ProfilComponent,
    TakasComponent,
    GirisComponent


  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
