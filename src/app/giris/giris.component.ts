import { Component, OnInit } from '@angular/core';
import {kullaniciDB} from "../models/kullanici";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {kullaniciService} from "../Service/kullaniciService";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-giris',
  templateUrl: './giris.component.html',
  styleUrls: ['./giris.component.css'],
  providers: [kullaniciService]
})
export class GirisComponent implements OnInit {

  kullanici : kullaniciDB[] = [];

  constructor(private kullaniciService:kullaniciService,
              private http: HttpClient,
              private route: Router) { }

  ngOnInit(): void {
    this.kullaniciService.getKullanici().subscribe(data => {
      this.kullanici = data;
      console.log(data);
    })
  }
  kullaniciKayit = new FormGroup({
    kullaniciAd: new FormControl('',[Validators.required]),
    kullaniciSoyad: new FormControl('',[Validators.required]),
    kullaniciNick: new FormControl('',[Validators.required]),
    kullaniciDT: new FormControl('',[Validators.required]),
    kullaniciSifre: new FormControl('',Validators.required)

  });
  kayitFonk(data:any) {
     this.kullaniciService.createKullanici(data).subscribe(data=>{
       console.log(data);
       this.route.navigate(['/giris']);
       console.log("Kayıt gönderildi");
     });
  }

}

