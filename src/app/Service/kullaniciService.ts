import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {kullaniciDB} from "../models/kullanici";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class kullaniciService {

  url = "http://localhost:3000/kullanici";

  constructor(private http: HttpClient) { }

  getKullanici(): Observable<kullaniciDB[]> {
    return  this.http.get<kullaniciDB[]>(this.url);
  }

  createKullanici(kullanici:kullaniciDB): Observable<kullaniciDB> {
    const httpOptions = {
      headers : new HttpHeaders({
        'content-type' : 'application/json',
        'Authorization' : 'Token'
      })
    }
    return this.http.post<kullaniciDB>(this.url,kullanici,httpOptions);
  }
}
