import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KutuphaneComponentComponent } from './kutuphane-component.component';

describe('KutuphaneComponentComponent', () => {
  let component: KutuphaneComponentComponent;
  let fixture: ComponentFixture<KutuphaneComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KutuphaneComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KutuphaneComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
