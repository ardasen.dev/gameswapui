import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes,RouterModule} from "@angular/router";
import {AnasayfaComponentComponent} from './anasayfa-component/anasayfa-component.component';
import {AppComponent} from './app.component';
import {GirisComponent} from "./giris/giris.component";

const routes : Routes = [
  {
    path:'giris',
    component:GirisComponent
  },
  {
    path:'',
    redirectTo : 'giris',
    pathMatch:'full'
  },

  {
    path:'approute',
    component:AppComponent
  },
  {
    path:'anasayfaroute',
    component:AnasayfaComponentComponent
  }
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
